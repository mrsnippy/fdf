/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: docker <docker@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 12:21:29 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/28 15:54:16 by docker           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <fcntl.h>
# include <stdio.h>
# include "libft.h"
# include <mlx.h>
# include <math.h>
# define WIN_W 1280
# define WIN_H 720

typedef struct	s_crd
{
	float		x;
	float		y;
	float		z;
	int			color;
}				t_crd;

typedef struct	s_dim
{
	int			h;
	int			w;
	float		zm;
	float		step;
	int			al;
	int			b;
}				t_dim;

typedef struct	s_mlx
{
	void		*init;
	void		*win;
	void		*menu;
	void		*img;
}				t_mlx;

typedef struct	s_fdf
{
	t_crd		***res;
	t_crd		***m;
	t_dim		*dim;
	t_mlx		*mlx;
	int			p_f;
}				t_fdf;

t_crd			***ft_map_split(char **map, t_dim **dim);
t_crd			***ft_change_z(t_crd ***m, t_dim *d, int flag);

int				ft_atoi_base(char *str, int base);

int				ft_error(void);
int				ft_arrdeep(char **str);
void			ft_letters(t_mlx *p);

void			ft_painter(t_crd ***crd_map, t_dim *dim, char *name);
void			ft_redraw(t_fdf *p);
void			ft_brzn(t_crd ***m, t_dim *dim, t_mlx *mlx, int flag);
void			ft_i(int x, int y, int rgb, t_mlx *mlx);

void			ft_proj(t_crd ***m, t_dim *dim, t_crd ****r, int flag);

void			ft_z(int keycode, t_fdf *p);
void			ft_displace(t_fdf *p, int flag);
void			ft_pro_and_zoom(int keycode, t_fdf *p, int flag);
void			ft_zoom(int keycode, t_fdf *p);
void			ft_rotate(int keycode, t_fdf *p);

#endif
