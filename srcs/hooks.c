/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: docker <docker@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 18:24:53 by snippy            #+#    #+#             */
/*   Updated: 2017/03/28 15:57:52 by docker           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_displace(t_fdf *p, int flag)
{
	int i;
	int j;

	i = -1;
	while (++i < p->dim->h)
	{
		j = -1;
		while (++j < p->dim->w)
		{
			if (flag == 1)
				p->res[i][j]->y -= 10;
			if (flag == 2)
				p->res[i][j]->y += 10;
			if (flag == 3)
				p->res[i][j]->x += 10;
			if (flag == 4)
				p->res[i][j]->x -= 10;
		}
	}
	ft_redraw(p);
}

void	ft_pro_and_zoom(int keycode, t_fdf *p, int flag)
{
	t_crd	***r;

	r = (t_crd ***)malloc(sizeof(t_crd **) * p->dim->h);
	if (flag == 0)
	{
		(keycode == 34) ? ft_proj(p->m, p->dim, &r, 1) :
			ft_proj(p->m, p->dim, &r, 0);
		p->p_f = (keycode == 34) ? 1 : 0;
	}
	if (flag == 1)
	{
		if (keycode == 69 && p->dim->zm <= 38)
			p->dim->zm += p->dim->step;
		if (keycode == 78)
			p->dim->zm -= p->dim->step;
		if (p->p_f == 1)
			ft_proj(p->m, p->dim, &r, 1);
		else
			ft_proj(p->m, p->dim, &r, 0);
	}
	p->res = r;
	ft_redraw(p);
}

/*void	ft_zoom(int keycode, t_fdf *p)
{
	t_crd	***r;

	free(p->res);
	r = (t_crd ***)malloc(sizeof(t_crd **) * p->dim->h);
	if (keycode == 69 && p->dim->zm <= 38)
		p->dim->zm += p->dim->step;
	if (keycode == 78)
		p->dim->zm -= p->dim->step;
	if (p->p_f == 1)
		ft_proj(p->m, p->dim, &r, 1);
	else
		ft_proj(p->m, p->dim, &r, 0);
	p->res = r;
	ft_redraw(p);
}*/

void	ft_rotate(int keycode, t_fdf *p)
{
	t_crd	***r;

	r = (t_crd ***)malloc(sizeof(t_crd **) * p->dim->h);
	if (keycode == 83)
		p->dim->al += 10;
	if (keycode == 84)
		p->dim->b += 10;
	if (p->p_f == 1)
		ft_proj(p->m, p->dim, &r, 1);
	else
		ft_proj(p->m, p->dim, &r, 0);
	p->res = r;
	ft_redraw(p);
}

int			ft_error(void)
{
	ft_putendl("error");
	return (0);
}

int		ft_arrdeep(char **s)
{
	int	j;

	j = -1;
	while (s[++j])
		;
	return (j);
}
