/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   changez.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: docker <docker@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 19:32:24 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/28 14:25:07 by docker           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static float	ft_change_z_sup(t_crd ***m, t_dim *d)
{
	int		i;
	int		j;
	float	max;
	float	min;
	float	mmz;

	i = 0;
	max = m[0][0]->z;
	min = m[0][0]->z;
	while (++i < d->h)
	{
		j = 0;
		while (++j < d->w)
		{
			max = (m[i][j]->z > max) ? m[i][j]->z : max;
			min = (m[i][j]->z < min) ? m[i][j]->z : min;
		}
	}
	mmz = (max - min) * 0.1;
	return (mmz);
}

void			ft_change_z_sup2(float *z, float *mmz, int flag)
{
	if (flag == 0)
	{
		if ((*z = *z - *mmz) == 0)
			*z = *z - *mmz;
	}
	if (flag == 1)
	{
		if ((*z = *z + *mmz) == 0)
			*z = *z + *mmz;
	}
}

t_crd			***ft_change_z(t_crd ***m, t_dim *d, int flag)
{
	int		i;
	int		j;
	float	mmz;

	mmz = ft_change_z_sup(m, d);
	i = -1;
	while (++i < d->h)
	{
		j = -1;
		while (++j < d->w)
		{
			if (m[i][j]->z != 0)
				ft_change_z_sup2(&(m[i][j]->z), &mmz, flag);
		}
	}
	return (m);
}

void	ft_z(int keycode, t_fdf *p)
{
	t_crd	***r;

	free(p->res);
	r = (t_crd ***)malloc(sizeof(t_crd **) * p->dim->h);
	if (keycode == 6)
		p->m = ft_change_z(p->m, p->dim, 1);
	else
		p->m = ft_change_z(p->m, p->dim, 0);
	(p->p_f == 1) ? ft_proj(p->m, p->dim, &r, 1) :
		ft_proj(p->m, p->dim, &r, 0);
	p->res = r;
	ft_redraw(p);
}
