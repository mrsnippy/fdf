/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   painter.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: docker <docker@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/04 13:11:49 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/28 15:55:48 by docker           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_hooks(int keycode, t_fdf *p)
{
	if (keycode == 53)
		exit(0);
	if ((keycode == 6 || keycode == 7) && p->p_f != 2)
		ft_z(keycode, p);
	if (keycode == 126 && p->p_f != 2)
		ft_displace(p, 1);
	if (keycode == 125 && p->p_f != 2)
		ft_displace(p, 2);
	if (keycode == 124 && p->p_f != 2)
		ft_displace(p, 3);
	if (keycode == 123 && p->p_f != 2)
		ft_displace(p, 4);
	if (keycode == 34 || keycode == 35)
		ft_pro_and_zoom(keycode, p, 0);
	if ((keycode == 69 || keycode == 78) && p->p_f != 2)
		ft_pro_and_zoom(keycode, p, 1); 
	if ((keycode == 83 || keycode == 84) && p->p_f != 2)
		ft_rotate(keycode, p);
	return (0);
}

void		ft_letters(t_mlx *p)
{
	mlx_string_put(p->init, p->menu, 20, 20, 16777215, "MENU:");
	mlx_string_put(p->init, p->menu, 20, 60, 16773120,
		"i - for ISOMETRIC projection");
	mlx_string_put(p->init, p->menu, 20, 80, 16773120,
		"p - for PARALLEL projection");
	mlx_string_put(p->init, p->menu, 20, 140, 6750207,
		"Use arrow keys to move the picture");
	mlx_string_put(p->init, p->menu, 20, 160, 6750207,
		"Use +/- keys to zoom in and out");
	mlx_string_put(p->init, p->menu, 20, 180, 6750207,
		"Use z/x keys to change Z axis coordinates");
	mlx_string_put(p->init, p->menu, 20, 200, 6750207,
		"Use Num1/Num2  keys to change view angles");
}

void		ft_redraw(t_fdf *p)
{
	int i;
	int j;

	mlx_destroy_image(p->mlx->init, p->mlx->img);
	p->mlx->img = mlx_new_image(p->mlx->init, WIN_W, WIN_H);
	i = -1;
	while (++i < p->dim->h)
	{
		j = -1;
		while (++j < p->dim->w)
			ft_i(p->res[i][j]->x, p->res[i][j]->y, p->res[i][j]->color, p->mlx);
	}
	ft_brzn(p->res, p->dim, p->mlx, 0);
	ft_brzn(p->res, p->dim, p->mlx, 1);
	mlx_put_image_to_window(p->mlx->init, p->mlx->win, p->mlx->img, 0, 0);
}

void		ft_painter(t_crd ***crd_map, t_dim *dim, char *n)
{
	t_mlx		*mlx;
	t_fdf		*p;

	mlx = (t_mlx *)malloc(sizeof(t_mlx));
	mlx->init = mlx_init();
	mlx->win = mlx_new_window(mlx->init, WIN_W, WIN_H, ft_strjoin("fdf: ", n));
	mlx->menu = mlx_new_window(mlx->init, 300, 300, "menu");
	ft_letters(mlx);
	mlx->img = mlx_new_image(mlx->init, WIN_W, WIN_H);
	p = (t_fdf *)malloc(sizeof(t_fdf));
	p->dim = dim;
	p->mlx = mlx;
	p->p_f = 2;
	p->dim->al = 300;
	p->dim->b = 300;
	p->m = crd_map;
	ft_pro_and_zoom(34, p, 0);
	mlx_key_hook(mlx->win, ft_hooks, p);
	mlx_loop(mlx->init);
}
