/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elton.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: docker <docker@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 12:51:04 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/28 13:56:19 by docker           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_i(int x, int y, int rgb, t_mlx *mlx)
{
	int				bpp;
	int				sl;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(mlx->img, &bpp, &sl, &en);
	tmp = (mlx_get_color_value(mlx->init, rgb));
	if (x > 0 && x < WIN_W && y > 0 && y < WIN_H)
	{
		ft_memcpy((void *)(image + sl * y + x * sizeof(int)),
			(void *)&tmp, 4);
	}
}

static void	ft_john(float **ds, float **err, int **x, int **y)
{
	(*ds)[0] = abs((*x)[1] - (*x)[0]);
	(*ds)[1] = abs((*y)[1] - (*y)[0]);
	(*ds)[2] = ((*x)[0] < (*x)[1]) ? 1 : -1;
	(*ds)[3] = ((*y)[0] < (*y)[1]) ? 1 : -1;
	(*err)[0] = (*ds)[0] - (*ds)[1];
}

static void	ft_elton(int *x, int *y, int color, t_mlx *mlx)
{
	float	*ds;
	float	*err;

	err = (float *)malloc(sizeof(float) * 2);
	ds = (float *)malloc(sizeof(float) * 4);
	ft_john(&ds, &err, &x, &y);
	while (x[0] != x[1] || y[0] != y[1])
	{
		err[1] = err[0] * 2;
		if (err[1] > -ds[1])
		{
			err[0] -= ds[1];
			x[0] += ds[2];
		}
		if (err[1] < ds[0])
		{
			err[0] += ds[0];
			y[0] += ds[3];
		}
		ft_i(x[0], y[0], color, mlx);
	}
	free(err);
	free(ds);
}

static void	ft_norma_dura(int **xx, int **yy, int flag)
{
	if (flag == 0)
	{
		*xx = (int *)malloc(sizeof(int) * 2);
		*yy = (int *)malloc(sizeof(int) * 2);
	}
	if (flag == 1)
	{
		free(*xx);
		free(*yy);
	}
}

void		ft_brzn(t_crd ***m, t_dim *dim, t_mlx *mlx, int flag)
{
	int i;
	int	j;
	int *xx;
	int *yy;

	i = -1;
	j = -1;
	while ((flag == 0) ? ++i < dim->h : ++j < dim->w)
	{
		if (flag == 0)
			j = -1;
		else
			i = -1;
		while ((flag == 0) ? ++j < dim->w - 1 : ++i < dim->h - 1)
		{
			ft_norma_dura(&xx, &yy, 0);
			xx[0] = m[i][j]->x;
			xx[1] = (flag == 0) ? m[i][j + 1]->x : m[i + 1][j]->x;
			yy[0] = m[i][j]->y;
			yy[1] = (flag == 0) ? m[i][j + 1]->y : m[i + 1][j]->y;
			ft_elton(xx, yy, m[i][j]->color, mlx);
			ft_norma_dura(&xx, &yy, 1);
		}
	}
}
