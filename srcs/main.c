/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: docker <docker@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 15:08:04 by dmurovts          #+#    #+#             */
/*   Updated: 2017/03/28 16:02:52 by docker           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		ft_newcrd(float x, float y, char *str, t_crd **list)
{
	char	**arr;

	*list = (t_crd *)malloc(sizeof(t_crd));
	(*list)->x = x;
	(*list)->y = y;
	arr = ft_strsplit(str, ',');
	(*list)->z = ft_atoi(ft_strdup(arr[0]));
	(*list)->color = (!arr[1]) ? ft_atoi_base("FFFFFF", 16) :
		ft_atoi_base(ft_strsub(arr[1], 2, 6), 16);
	free(arr);
}

t_crd			***ft_map_split(char **map, t_dim **dim)
{
	int			i;
	int			j;
	t_crd		***res;
	char		**tmp;

	(*dim)->h = ft_arrdeep(map);
	res = (t_crd ***)malloc(sizeof(t_crd **) * (*dim)->h);
	i = -1;
	while (map[++i])
	{
		tmp = ft_strsplit(map[i], ' ');
		(*dim)->w = (i == 0) ? ft_arrdeep(tmp) : (*dim)->w;
		if (ft_arrdeep(tmp) != (*dim)->w)
			return (NULL);
		res[i] = (t_crd **)malloc(sizeof(t_crd *) * (*dim)->w);
		j = -1;
		while (++j < ft_arrdeep(tmp))
			ft_newcrd(j, i, tmp[j], &res[i][j]);
		free(tmp);
	}
	return (res);
}

static int		ft_lines(int fd, int *j)
{
	int		i;
	char	*tmp;
	int		gnl;

	i = 0;
	while ((gnl = get_next_line(fd, &tmp)))
	{
		if (gnl > 0)
		{
			*j = (i == 0) ? ft_strlen(tmp) : *j;
			free(tmp);
			i++;
		}
		else if (gnl < 0)
			return (-1);
	}
	close(fd);
	return (i);
}

static int		ft_getcrd(int fd, char ***map, char *name)
{
	char	*tmp;
	int		gnl;
	int		i;
	int		j;

	j = 0;
	if ((i = ft_lines(fd, &j)) <= 0 ||
		!(*map = (char **)malloc(sizeof(char *) * (i + 1))))
		return (0);
	fd = open(name, O_RDONLY);
	i = -1;
	while ((gnl = get_next_line(fd, &tmp)))
	{
		if (gnl > 0)
		{
			(*map)[++i] = (char *)malloc(sizeof(char *) * (j + 1));
			(*map)[i] = ft_strdup(tmp);
			free(tmp);
		}
		else if (gnl < 0)
			return (0);
	}
	(*map)[++i] = NULL;
	close(fd);
	return (1);
}

int				main(int argc, char **argv)
{
	int			fd;
	char		**u_map;
	t_crd		***crd_map;
	t_dim		*dim;

	if (argc == 2)
	{
		if ((fd = open(argv[1], O_RDONLY)) < 0)
			return (ft_error());
		if (fd < 0 || !ft_getcrd(fd, &u_map, argv[1]))
			return (ft_error());
		dim = (t_dim *)malloc(sizeof(t_dim));
		if (!(crd_map = ft_map_split(u_map, &dim)))
			return (ft_error());
		dim->zm = WIN_W / 5 / dim->w;
		dim->zm = (int)dim->zm == 0 ? 0.5 : dim->zm;
		dim->step = dim->zm / 5;
		free(u_map);
		ft_painter(crd_map, dim, argv[1]);
		return (0);
	}
	return (ft_error());
}
