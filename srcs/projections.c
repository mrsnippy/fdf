/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projections.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: docker <docker@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 19:02:28 by snippy            #+#    #+#             */
/*   Updated: 2017/03/28 15:49:32 by docker           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static float	ft_rad(int a)
{
	return (a * M_PI / 180);
}

static void		ft_pariso(t_crd *m, t_dim *dim, t_crd *r, int flag)
{	
	if (flag == 0)
	{
		r->x = m->x;
		r->x = (r->x - dim->w / 2) * dim->zm + WIN_W / 2;
		r->y = m->y * cos(ft_rad(dim->al))
			+ m->z * sin(ft_rad(dim->al));
		r->y = (r->y - dim->h / 2) * dim->zm + WIN_H / 2;
		r->color = m->color;
	}
	if (flag == 1)
	{
		r->x = (m->y - m->x) * sin(ft_rad(dim->al));
		r->x = (r->x - dim->w / 2) * dim->zm + WIN_W / 2;
		r->y = ((m->x + m->y) * cos(ft_rad(dim->b))
			- m->z);
		r->y = (r->y - dim->h / 2) * dim->zm + WIN_H / 2;
		r->color = m->color;
	}
}

void			ft_proj(t_crd ***m, t_dim *dim, t_crd ****r, int flag)
{
	int			i;
	int			j;

	i = -1;
	while (++i < dim->h)
	{
		j = -1;
		(*r)[i] = (t_crd **)malloc(sizeof(t_crd *) * dim->w);
		while (++j < dim->w)
		{
			(*r)[i][j] = (t_crd *)malloc(sizeof(t_crd));
			ft_pariso(m[i][j], dim, (*r)[i][j], flag);
		}
	}
}
