NAME = fdf

HEAD = includes/

VPATH = srcs:includes

FLAGS = -Wall -Wextra -I $(HEAD)

MLX = -lmlx -lXext -lX11 -lm

SRCS = main.c									\
		ft_atoi_base.c							\
		painter.c								\
		hooks.c 								\
		projections.c 							\
		elton.c 								\
		changez.c 								\

BINS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(BINS)
	 make re -C libft/ fclean && make -C libft/
	gcc -o $(NAME) -I libft/includes $(BINS) $(FLAGS) -L. libft/libft.a $(MLX)

%.o: %.c
	gcc -I libft/includes $(FLAGS) -c -o $@ $<

clean:
	/bin/rm -f $(BINS)

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all
